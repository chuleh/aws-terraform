# Main networking terraform file
# Here's where the VPC, subnets, IGW and route table will be defined

resource "aws_vpc" "lcasaretto_vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags {
    Name = "Main vpc for lcasaretto"
  }
}

# two public subnets as it will have two instances
# subnet 1
resource "aws_subnet" "main_public_1" {
  vpc_id                  = "${aws_vpc.lcasaretto_vpc.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2a"

  tags {
    Name = "public subnet 1"
  }
}

# subnet 2
resource "aws_subnet" "main_public_2" {
  vpc_id                  = "${aws_vpc.lcasaretto_vpc.id}"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2d"

  tags {
    Name = "public subnet 2"
  }
}

# Internet gateway
resource "aws_internet_gateway" "main_gw" {
  vpc_id = "${aws_vpc.lcasaretto_vpc.id}"

  tags {
    Name = "Main gateway"
  }
}

# route tables
resource "aws_route_table" "main_public_rt" {
  vpc_id = "${aws_vpc.lcasaretto_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main_gw.id}"
  }

  tags {
    Name = "Main public rt"
  }
}

# route table associations
resource "aws_route_table_association" "main_public_1a" {
  subnet_id      = "${aws_subnet.main_public_1.id}"
  route_table_id = "${aws_route_table.main_public_rt.id}"
}

resource "aws_route_table_association" "main_public_2f" {
  subnet_id      = "${aws_subnet.main_public_2.id}"
  route_table_id = "${aws_route_table.main_public_rt.id}"
}
