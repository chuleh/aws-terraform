# main file with variables to create the aws infra

variable "AWS_ACCESS_KEY" {}

variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
  default = "us-west-2"
}

variable "AMI" {
  default = "ami-06d990a360d7a793c"
}
